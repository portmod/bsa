import os
from typing import Dict, Set, Tuple

from portmod.globals import env
from portmod.loader import load_all_installed
from portmod.tsort import CycleException, tsort
from portmodlib.atom import Atom, atom_sat
from portmodlib.usestr import use_reduce

from configtool import read_userconfig, usedep_matches_installed


def sort_vfs():
    """
    Sorts VFS based on package names
    """
    env.set_prefix(os.environ["PORTMOD_PREFIX_NAME"])

    installed = list(load_all_installed())

    graph: Dict[Tuple[str, str, bool], Set[Tuple[str, str, bool]]] = {}
    priorities = {}

    user_config_path = os.path.join(
        os.environ["PORTMOD_CONFIG_DIR"], "config", "install.csv"
    )

    case_insensitive = bool(os.environ.get("CASE_INSENSITIVE_FILES", False))
    # Keys refer to masters (overridden).
    # values are a set of overriding files
    userconfig: Dict[str, Set[str]] = read_userconfig(
        user_config_path, case_insensitive
    )

    # Determine all Directories that are enabled
    for pkg in installed:
        if pkg._PYBUILD_VER == 1:
            graph[pkg.CPN] = set()
            priorities[pkg.CPN] = pkg.TIER

    # Add edges in the graph for each data override
    for pkg in installed:
        if pkg._PYBUILD_VER != 1:
            continue
        parents = set(
            use_reduce(
                pkg.DATA_OVERRIDES + " " + pkg.DATA_OVERRIDES,
                pkg.INSTALLED_USE,
                flat=True,
                token_class=Atom,
            )
        ) | {
            Atom(override)
            for name in userconfig
            for override in userconfig[name]
            if atom_sat(pkg.ATOM, Atom(name))
        }

        for parent in parents:
            if not usedep_matches_installed(parent):
                continue

            for atom in graph:
                if atom_sat(Atom(atom), parent):
                    if Atom(atom).BLOCK:
                        # Blockers have reversed edges
                        graph[pkg.CPN].add(atom)
                    else:
                        graph[atom].add(pkg.CPN)
    try:
        sorted_pkgs = tsort(graph, priorities)
    except CycleException as error:
        raise CycleException(
            "encountered cycle when sorting VFS!", error.cycle
        ) from error

    return sorted_pkgs
